package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.endpoint.IUserEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public @NotNull User createUser(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password);
    }

    @Override
    @WebMethod
    public @NotNull User createUserWithEmail(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "email", partName = "email") final String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createUserWithRole(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password,
            @WebParam(name = "role", partName = "role") final Role role
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().create(login, password, role);
    }

    @Override
    @WebMethod
    public void changeUserPassword(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "passwordOld", partName = "passwordOld") final String passwordOld,
            @WebParam(name = "passwordNew", partName = "passwordNew") final String passwordNew,
            @WebParam(name = "userId", partName = "userId") final Long userId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changePassword(passwordOld, passwordNew, userId);
    }

    @Override
    @WebMethod
    public void profileOfUserChange(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "email", partName = "email") final String email,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "LastName", partName = "LastName") final String LastName,
            @WebParam(name = "userId", partName = "userId") final Long userId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().changeUser(email, firstName, LastName, userId);
    }

}
