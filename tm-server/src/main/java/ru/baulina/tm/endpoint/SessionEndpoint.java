package ru.baulina.tm.endpoint;

import ru.baulina.tm.api.endpoint.ISessionEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint() {
        super();
    }

    public SessionEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return serviceLocator.getSessionService().open(login, password);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void closeAllSession(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().closeAll(session);
    }

    @Override
    @WebMethod
    public User getUser(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        return serviceLocator.getSessionService().getUser(session);
    }

    @Override
    @WebMethod
    public List<Session> getListSession(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        return serviceLocator.getSessionService().getListSession(session);
    }

}
