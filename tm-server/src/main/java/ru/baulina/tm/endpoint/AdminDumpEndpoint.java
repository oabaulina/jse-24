package ru.baulina.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.baulina.tm.api.endpoint.IAdminDampEndpoint;
import ru.baulina.tm.api.service.*;
import ru.baulina.tm.constant.DataConstant;
import ru.baulina.tm.dto.Domain;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.data.DataDeleteException;
import ru.baulina.tm.exception.data.DataLoadException;
import ru.baulina.tm.exception.data.DataSaveException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.*;
import java.nio.file.Files;
import java.util.Base64;

@WebService
public class AdminDumpEndpoint implements IAdminDampEndpoint {

    private IServiceLocator serviceLocator;

    public AdminDumpEndpoint() {
    }

    public AdminDumpEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void dataBase64Clear(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_BASE64);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
    }

    @Override
    @WebMethod
    public void dataBase64Load(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_BASE64);
        try {
            final byte[] bytesEncoded = Files.readAllBytes(file.toPath());
            final byte[] bytes = Base64.getDecoder().decode(bytesEncoded);
            final ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
            final ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);
            final Domain domain = (Domain) objectInputStream.readObject();
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            objectInputStream.close();
            byteInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new DataLoadException(e);
        }
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void dataBase64Save(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_BASE64);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
                final ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream)
        ) {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
            objectOutputStream.writeObject(domain);
            final byte[] bytes = byteOutputStream.toByteArray();
            final String Base64Encoded = Base64.getEncoder().encodeToString(bytes);
            final byte[] bytesEncoded = Base64Encoded.getBytes();

            fileOutputStream.write(bytesEncoded);
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }
    }

    @Override
    @WebMethod
    public void dataBinaryClear(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
    }

    @Override
    @WebMethod
    public void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_BINARY);
        try (
                final FileInputStream fileInputStream = new FileInputStream(file);
                final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            final Domain domain = (Domain) objectInputStream.readObject();
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
        } catch (IOException | ClassNotFoundException e) {
            throw new DataLoadException(e);
        }
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void dataBinarySave(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_BINARY);
        try {
            Files.deleteIfExists(file.toPath());
            Files.createFile(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        ){
            objectOutputStream.writeObject(domain);
        } catch (IOException e) {
            e.fillInStackTrace();
            throw new DataSaveException(e);
        }
    }

    @Override
    @WebMethod
    public void dataJasonClear(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_JSON);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
    }

    @Override
    @WebMethod
    public void dataJasonLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_JSON);
        try (
                final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void dataJasonSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_JSON);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }
    }

    @Override
    @WebMethod
    public void dataXmlClear(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_XML);
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            throw new DataDeleteException();
        }
    }

    @Override
    @WebMethod
    public void dataXmlLoad(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final File file = new File(DataConstant.FILE_XML);
        try (
                final FileInputStream fileInputStream = new FileInputStream(file)
        ) {
            final ObjectMapper objectMapper = new XmlMapper();
            final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
            final IProjectService projectService = serviceLocator.getProjectService();
            final ITaskService taskService = serviceLocator.getTaskService();
            final IUserService userService = serviceLocator.getUserService();
            taskService.load(domain.getTasks());
            userService.load(domain.getUsers());
            projectService.load(domain.getProjects());
        } catch (IOException e) {
            throw new DataLoadException(e);
        }
        serviceLocator.getSessionService().close(session);
    }

    @Override
    @WebMethod
    public void dataXmlSave(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);

        final IDomainService domainService = serviceLocator.getDomainService();
        final Domain domain = new Domain();
        domainService.export(domain);
        final File file = new File(DataConstant.FILE_XML);
        try (
                final FileOutputStream fileOutputStream = new FileOutputStream(file);
        ) {
            final ObjectMapper objectMapper = new XmlMapper();
            final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new DataSaveException(e);
        }
    }

}