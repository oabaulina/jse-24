package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    @Override
    public List<E> getList() {
        return entities;
    }

    @Override
    public void merge(@Nullable final List<E> entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    public void merge(@Nullable final E... entities) {
        if (entities == null) return;
        for (final E entity: entities) merge(entity);
    }

    @Override
    public void merge(@Nullable final E entity) {
        if (entity == null) return;
        final int isEntities = entities.indexOf(entity);
        if (isEntities != -1) {return;}
        else {entities.add(entity);}
    }

    @Override
    public void load(@NotNull final List<E> entities) {
        clear();
        merge(entities);
    }

    @Override
    public void load(@NotNull final E... entities) {
        clear();
        merge(entities);
    }

    @Override
    public void clear() {
        entities.clear();
    }

}
