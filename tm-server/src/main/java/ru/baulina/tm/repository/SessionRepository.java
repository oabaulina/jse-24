package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ISessionRepository;
import ru.baulina.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(@NotNull final Session session) {
        merge(session);
    }

    @Override
    public void remove(@NotNull final Session session) {
        @NotNull final List<Session> sessions = getList();
        @NotNull Long id = session.getId();
        @Nullable final Session sessionDel = findById(id);
        sessions.remove(sessionDel);
    }

    @Override
    public void removeByUserId(@NotNull final Long userId) {
        @Nullable final List<Session> sessions = findByUserId(userId);
        for (@NotNull final Session session: sessions) {
            remove(session);
        }
    }

    @NotNull
    @Override
    public List<Session> findByUserId(@NotNull final Long userId) {
        @NotNull final List<Session> result = new ArrayList<>();
        @NotNull final List<Session> sessions = getList();
        for (@NotNull final Session session: sessions) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @Nullable
    @Override
    public Session findById(@NotNull final Long id) {
        @NotNull final List<Session> sessions = getList();
        for (@NotNull final Session session: sessions) {
            if (id.equals(session.getId())) return session;
        }
        return null;
    }

    @Override
    public boolean contains(@NotNull final Long id) {
        @Nullable final Session session = findById(id);
        return getList().contains(session);
    }

}
