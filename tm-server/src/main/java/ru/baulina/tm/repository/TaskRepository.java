package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(@NotNull final Long userId, @NotNull final Task task) {
        task.setUserId(userId);
        merge(task);
    }

    @Override
    public void remove(@NotNull final Long userId, @NotNull final Task task) {
        @NotNull final List<Task> tasks = getList();
        @NotNull Long id = task.getId();
        @Nullable final Task taskDeleted = findOneById(userId, id);
        tasks.remove(taskDeleted);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final Long userId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final List<Task> tasks = getList();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(@NotNull final Long userId) {
        @NotNull final List<Task> tasks = getList();
        tasks.removeIf(task -> userId.equals(task.getUserId()));
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final Long userId, @NotNull final Long id) {
        @NotNull final List<Task> tasks = getList();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final List<Task> tasks = getList();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final Long userId, @NotNull final String name) {
        @NotNull final List<Task> tasks = getList();
        for (@NotNull final Task task: tasks) {
            if (userId.equals(task.getUserId()) && name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final Long userId, @NotNull final Long id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        @NotNull final List<Task> tasks = getList();
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final Long userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}
