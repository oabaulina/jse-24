package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.service.IPropertyService;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    public void init() throws Exception {
        try (InputStream inputStream = PropertyService.class.getResourceAsStream(NAME)) {
            properties.load(inputStream);
        }
    }

    @Nullable
    @Override
    public String getServiceHost() {
        @Nullable final String propertyHost = properties.getProperty("server.host");
        @Nullable final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Nullable
    @Override
    public Integer getServicePort() {
        @Nullable final String propertyPort = properties.getProperty("server.port");
        @Nullable final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Nullable
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Nullable
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

}
