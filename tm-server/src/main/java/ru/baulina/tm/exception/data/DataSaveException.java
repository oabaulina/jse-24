package ru.baulina.tm.exception.data;

public class DataSaveException extends RuntimeException {

    public DataSaveException(Throwable throwable) {
        super("Error! Problem with saving file...", throwable);
    }

}
