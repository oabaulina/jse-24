package ru.baulina.tm.api.service;

public interface IPropertyService {

    String getServiceHost();

    Integer getServicePort();

    String getSessionSalt();

    Integer getSessionCycle();

}
