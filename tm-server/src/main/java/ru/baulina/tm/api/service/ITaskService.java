package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task create(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Task create(
            @Nullable final Long userId, @Nullable final String name,
            @Nullable final String description
    );

    Task add(@Nullable final Long userId, @Nullable final Task task);

    void remove(@Nullable final Long userId, @Nullable final Task task);

    @NotNull
    List<Task> findAll(@Nullable final Long userId);

    void clear(@Nullable final Long userId);

    @Nullable
    Task findOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Task findOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Task findOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Task removeOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Task removeOneByIndex(@Nullable final Long userId, @Nullable final Integer index);

    @Nullable
    Task removeOneByName(@Nullable final Long userId, @Nullable final String name);

    @Nullable
    Task updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    );

    @Nullable
    Task updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String  description
    );

}
