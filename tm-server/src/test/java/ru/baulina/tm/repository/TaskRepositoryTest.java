package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitRepositoryTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

@Category({UnitTest.class, UnitRepositoryTest.class})
public class TaskRepositoryTest {

    @Rule
    @NotNull
    public final TestName testName = new TestName();

    @Rule
    @NotNull public final ExpectedException thrown = ExpectedException.none();

    @NotNull private Task task1;
    @NotNull private Task task2;

    @NotNull private final List<Task> actual = new ArrayList<>();

    @NotNull private final TaskRepository taskRepository = new TaskRepository();

    @NotNull private User userTest;
    @NotNull private User userAdmin;

    @Before
    public void setTasks() throws Exception {
        System.out.println(testName.getMethodName());

        userTest = new User();
        userTest.setId(12345L);
        userTest.setLogin("test");
        userTest.setPasswordHash("14jhd54");
        userTest.setRole(Role.USER);

        userAdmin = new User();
        userAdmin.setId(58621L);
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("kjh54yu");
        userAdmin.setRole(Role.ADMIN);

        task1 = new Task();
        task1.setId(222222L);
        task1.setUserId(12345L);
        task1.setName("task1");

        task2 = new Task();
        task2.setId(223323L);
        task2.setUserId(12345L);
        task2.setName("task2");

        actual.add(task1);
        actual.add(task2);

        taskRepository.add(12345L, task1);
        taskRepository.add(12345L, task2);
    }

    @After
    public void cleaTasks() throws Exception {
        taskRepository.clear();
    }

    @Test
    public void testMerge() {
        @NotNull final Task task3 = new Task();
        task3.setUserId(12345L);
        task3.setName("task3");
        taskRepository.merge(task3);
        actual.add(task3);
        Assert.assertEquals(actual, taskRepository.getList());
        taskRepository.merge(task1);
        taskRepository.merge(task2);
        Assert.assertEquals(actual, taskRepository.findAll(12345L));
    }

    @Test
    public void testMerge1() {
        @NotNull final Task task4 = new Task();
        task4.setUserId(12345L);
        task4.setName("task4");
        @NotNull final Task task5 = new Task();
        task5.setUserId(12345L);
        task5.setName("task5");
        @NotNull final List<Task> expected = new ArrayList<>();
        expected.add(task4);
        expected.add(task5);
        actual.add(task4);
        actual.add(task5);
        taskRepository.merge(expected);
        Assert.assertEquals(actual, taskRepository.findAll(12345L));
        Assert.assertEquals(0, taskRepository.findAll(58621L).size());
    }

    @Test
    public void testMerge2() {
        @NotNull final Task task6 = new Task();
        task6.setUserId(58621L);
        task6.setName("task6");
        taskRepository.merge(task6);
        Assert.assertEquals(1, taskRepository.findAll(58621L).size());
    }

    @Test
    public void testClear() {
        taskRepository.clear(12345L);
        Assert.assertFalse(taskRepository.getList().size() > 0);
    }

    @Test
    public void testAdd() {
        @NotNull final Task task3 = new Task();
        task3.setUserId(12345L);
        task3.setName("task3");
        Assert.assertEquals(2, taskRepository.getList().size());
        taskRepository.add(12345L, task3);
        Assert.assertEquals(3, taskRepository.getList().size());
        Assert.assertEquals(3, taskRepository.findAll(12345L).size());
        Assert.assertEquals(0, taskRepository.findAll(58621L).size());
        actual.add(task3);
        Assert.assertEquals(actual, taskRepository.findAll(12345L));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(2, taskRepository.getList().size());
        @NotNull final List<Task> expected = new ArrayList<>();
        expected.add(task2);
        taskRepository.remove(12345L, task1);
        Assert.assertEquals(1, taskRepository.getList().size());
        Assert.assertEquals(expected, taskRepository.findAll(12345L));
        taskRepository.remove(58621L, task2);
        Assert.assertEquals(expected, taskRepository.findAll(12345L));
        Assert.assertEquals(1, taskRepository.findAll(12345L).size());
        Assert.assertEquals(expected, taskRepository.getList());
    }

    @Test
    public void testFindAll() {
        @NotNull List<Task> expected = taskRepository.findAll(12345L);
        Assert.assertEquals(expected, actual);
        expected = taskRepository.findAll(58621L);
        Assert.assertEquals(0, expected.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final Long taskId = task1.getId();
        Assert.assertEquals(task1, taskRepository.findOneById(12345L, taskId));
        Assert.assertNull(taskRepository.findOneById(58621L, taskId));
        Assert.assertNull(taskRepository.findOneById(12345L, 123L));
    }

    @Test
    public void testFindOneByIndex() {
        Assert.assertEquals(task1, taskRepository.findOneByIndex(12345L, 0));
        Assert.assertEquals(task2, taskRepository.findOneByIndex(12345L, 1));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexException() {
        Assert.assertNull(taskRepository.findOneByIndex(58621L, 0));
        Assert.assertNull(taskRepository.findOneByIndex(58621L, 1));
        Assert.assertNull(taskRepository.findOneByIndex(12345L, 2));
    }

    @Test
    public void testFindOneByName() {
        Assert.assertNotEquals(task2, taskRepository.findOneByName(12345L, "task1"));
        Assert.assertNotEquals(task1, taskRepository.findOneByName(58621L, "task1"));
        Assert.assertNull(taskRepository.findOneByName(12345L, "task3"));
        Assert.assertNull(taskRepository.findOneByName(58621L, "task3"));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final List<Task> expected = new ArrayList<>();
        expected.add(task2);
        @NotNull final Long taskId = task1.getId();
        taskRepository.removeOneById(12345L, taskId);
        Assert.assertNull(taskRepository.findOneById(12345L,taskId));
        Assert.assertEquals(expected, taskRepository.findAll(12345L));
        Assert.assertEquals(expected, taskRepository.getList());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testRemoveOneByIndex() {
        @NotNull final List<Task> expected = new ArrayList<>();
        expected.add(task2);
        taskRepository.removeOneByIndex(12345L, 0);
        Assert.assertEquals(task2, taskRepository.findOneByIndex(12345L,0));
        Assert.assertNull(taskRepository.findOneByIndex(12345L,1));
        Assert.assertEquals(expected, taskRepository.findAll(12345L));
        Assert.assertEquals(expected, taskRepository.getList());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final List<Task> expected = new ArrayList<>();
        expected.add(task2);
        taskRepository.removeOneByName(12345L, "task1");
        Assert.assertNull(taskRepository.findOneByName(12345L, "task1"));
        Assert.assertEquals(expected, taskRepository.findAll(12345L));
        Assert.assertEquals(expected, taskRepository.getList());
    }

}
