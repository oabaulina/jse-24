package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitRepositoryTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

@Category({UnitTest.class, UnitRepositoryTest.class})
public class UserRepositoryTest {

    @Rule
    @NotNull public final TestName testName = new TestName();

    @NotNull private User user1;
    @NotNull private User user2;

    @NotNull private final List<User> actual = new ArrayList<>();

    @NotNull private final UserRepository userRepository = new UserRepository();

    @Before
    public void setUsers() throws Exception {
        System.out.println(testName.getMethodName());

        user1 = new User();
        user1.setId(12345L);
        user1.setLogin("test1");
        user1.setPasswordHash("14jhd54");
        user1.setRole(Role.USER);

        user2 = new User();
        user2.setId(58621L);
        user2.setLogin("test2");
        user2.setPasswordHash("kjh54yu");
        user2.setEmail("test2@test.ru");
        user2.setRole(Role.USER);

        actual.add(user1);
        actual.add(user2);

        userRepository.add(user1);
        userRepository.add(user2);
    }

    @After
    public void clearUsers() throws Exception {
        userRepository.clear();
    }

    @Test
    public void testMerge() {
        @NotNull final User user3 = new User();
        user3.setLogin("test3");
        user3.setPasswordHash("45gfh5y");
        user3.setRole(Role.USER);
        userRepository.merge(user3);
        actual.add(user3);
        Assert.assertEquals(actual, userRepository.getList());
        userRepository.merge(user1);
        userRepository.merge(user2);
        Assert.assertEquals(actual, userRepository.getList());
    }

    @Test
    public void testMerge1() {
        @NotNull final User user4 = new User();
        user4.setLogin("user4");
        user4.setPasswordHash("8hx475fv");
        user4.setRole(Role.USER);
        @NotNull final User user5 = new User();
        user5.setLogin("user5");
        user5.setPasswordHash("ds5huk4");
        user5.setRole(Role.USER);
        @NotNull final List<User> expected = new ArrayList<>();
        expected.add(user4);
        expected.add(user5);
        actual.add(user4);
        actual.add(user5);
        userRepository.merge(expected);
        Assert.assertEquals(actual, userRepository.getList());
    }

    @Test
    public void testClear() {
        userRepository.clear();
        Assert.assertFalse(userRepository.getList().size() > 0);
    }

    @Test
    public void testAdd() {
        @NotNull final User user3 = new User();
        user3.setLogin("test3");
        user3.setPasswordHash("45gfh5y");
        user3.setRole(Role.USER);
        Assert.assertEquals(2, userRepository.getList().size());
        userRepository.add(user3);
        Assert.assertEquals(3, userRepository.getList().size());
        actual.add(user3);
        Assert.assertEquals(actual, userRepository.getList());
    }

    @Test
    public void testFindAll() {
        @NotNull List<User> expected = userRepository.findAll();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFindById() {
        @NotNull final Long userId = user1.getId();
        Assert.assertEquals(user1, userRepository.findById(userId));
        Assert.assertNull(userRepository.findById(123L));
    }

    @Test
    public void testFindByLogin() {
        @NotNull final String login = user1.getLogin();
        Assert.assertEquals(user1, userRepository.findByLogin(login));
        Assert.assertNull(userRepository.findByLogin("test7"));
    }

    @Test
    public void testRemoveUser() {
        @NotNull List<User> expected = new ArrayList<>();
        expected.add(user2);
        Assert.assertEquals(userRepository.removeUser(user1), user1);
        Assert.assertEquals(expected, userRepository.getList());
    }

    @Test
    public void testRemoveById() {
        @NotNull List<User> expected = new ArrayList<>();
        expected.add(user2);
        @NotNull final Long userId = user1.getId();
        userRepository.removeById(userId);
        Assert.assertNull(userRepository.findById(userId));
        Assert.assertEquals(expected, userRepository.getList());
    }

    @Test
    public void testRemoveByLogin() {
        @NotNull List<User> expected = new ArrayList<>();
        expected.add(user2);
        @NotNull final String login = user1.getLogin();
        userRepository.removeByLogin(login);
        Assert.assertNull(userRepository.findByLogin(login));
        Assert.assertEquals(expected, userRepository.getList());
    }

}
