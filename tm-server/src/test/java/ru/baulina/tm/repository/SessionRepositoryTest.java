package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitRepositoryTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

@Category({UnitTest.class, UnitRepositoryTest.class})
public class SessionRepositoryTest {

    @Rule
    @NotNull public final TestName testName = new TestName();

    @NotNull private Session session1;
    @NotNull private Session session2;

    @NotNull private final List<Session> actual = new ArrayList<>();

    @NotNull private final SessionRepository sessionRepository = new SessionRepository();

    @NotNull private User userTest;
    @NotNull private User userAdmin;

    @Before
    public void setSessions() throws Exception {
        System.out.println(testName.getMethodName());

        userTest = new User();
        userTest.setLogin("test");
        userTest.setPasswordHash("test3");
        userTest.setRole(Role.USER);

        userAdmin = new User();
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("admin");
        userAdmin.setRole(Role.USER);

        session1 = new Session();
        session1.setUserId(userTest.getId());
        session1.setTimestamp(System.currentTimeMillis());
        session1.setSignature("gdf5484zr84dfg");

        session2 = new Session();
        session2.setUserId(userTest.getId());
        session2.setTimestamp(System.currentTimeMillis());
        session2.setSignature("zgz654dgf65zg");

        actual.add(session1);
        actual.add(session2);

        sessionRepository.add(session1);
        sessionRepository.add(session2);
    }

    @After
    public void clearSession() throws Exception {
        sessionRepository.clear();
    }

    @Test
    public void testMerge() {
        @NotNull final Session session3 = new Session();
        session3.setUserId(userTest.getId());
        session3.setTimestamp(System.currentTimeMillis());
        session3.setSignature("5lk4pl65");
        sessionRepository.merge(session3);
        actual.add(session3);
        Assert.assertEquals(actual, sessionRepository.getList());
        sessionRepository.merge(session1);
        sessionRepository.merge(session2);
        Assert.assertEquals(actual, sessionRepository.getList());
    }

    @Test
    public void testMerge1() {
        @NotNull final Session session4 = new Session();
        session4.setUserId(userTest.getId());
        session4.setTimestamp(System.currentTimeMillis());
        session4.setSignature("5shf5f4f4");
        @NotNull final Session session5 = new Session();
        session5.setUserId(userTest.getId());
        session5.setTimestamp(System.currentTimeMillis());
        session5.setSignature("vzd564zdr");
        @NotNull final List<Session> expected = new ArrayList<>();
        expected.add(session4);
        expected.add(session5);
        actual.add(session4);
        actual.add(session5);
        sessionRepository.merge(expected);
        Assert.assertEquals(actual, sessionRepository.getList());
    }

    @Test
    public void testClear() {
        sessionRepository.clear();
        Assert.assertFalse(sessionRepository.getList().size() > 0);
    }

    @Test
    public void testAdd() {
        @NotNull final Session session3 = new Session();
        session3.setUserId(userTest.getId());
        session3.setTimestamp(System.currentTimeMillis());
        session3.setSignature("fg56gss857g");
        Assert.assertEquals(2, sessionRepository.getList().size());
        sessionRepository.add(session3);
        Assert.assertEquals(3, sessionRepository.getList().size());
        actual.add(session3);
        Assert.assertEquals(actual, sessionRepository.getList());
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(2, sessionRepository.getList().size());
        @NotNull final List<Session> expected = new ArrayList<>();
        expected.add(session2);
        sessionRepository.remove(session1);
        Assert.assertEquals(1, sessionRepository.getList().size());
        Assert.assertEquals(expected, sessionRepository.getList());
    }

    @Test
    public void testFindByUserId() {
        @NotNull final List<Session> expected = new ArrayList<>();
        expected.add(session1);
        expected.add(session2);
        Assert.assertEquals(2, sessionRepository.findByUserId(userTest.getId()).size());
        Assert.assertEquals(0, sessionRepository.findByUserId(userAdmin.getId()).size());
        Assert.assertEquals(expected, sessionRepository.findByUserId(userTest.getId()));
    }

    @Test
    public void testFindById() {
        @NotNull final Long sessionId = session1.getId();
        Assert.assertEquals(session1, sessionRepository.findById(sessionId));
        Assert.assertNull(sessionRepository.findById(123L));
    }

    @Test
    public void testRemoveByUserId() {
        sessionRepository.removeByUserId(userAdmin.getId());
        Assert.assertEquals(2, sessionRepository.getList().size());
        sessionRepository.removeByUserId(userTest.getId());
        Assert.assertEquals(0, sessionRepository.getList().size());
    }

    @Test
    public void testContains() {
        Assert.assertTrue(sessionRepository.contains(session1.getId()));
        Assert.assertTrue(sessionRepository.contains(session2.getId()));
        @NotNull final Session session3 = new Session();
        session3.setUserId(userTest.getId());
        session3.setTimestamp(System.currentTimeMillis());
        session3.setSignature("5lk4pl65");
        Assert.assertFalse(sessionRepository.contains(session3.getId()));
    }

}
