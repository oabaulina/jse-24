package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitRepositoryTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.UnitUtilTest;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.List;

@Category({UnitTest.class, UnitRepositoryTest.class})
public class ProjectRepositoryTest {

    @Rule
    @NotNull
    public final TestName testName = new TestName();

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull private Project project1;
    @NotNull private Project project2;

    @NotNull private final List<Project> actual = new ArrayList<>();

    @NotNull private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull private User userTest;
    @NotNull private User userAdmin;

    @Before
    public void setProjects() throws Exception {
        System.out.println(testName.getMethodName());

        userTest = new User();
        userTest.setId(12345L);
        userTest.setLogin("test");
        userTest.setPasswordHash("14jhd54");
        userTest.setRole(Role.USER);

        userAdmin = new User();
        userAdmin.setId(58621L);
        userAdmin.setLogin("admin");
        userAdmin.setPasswordHash("kjh54yu");
        userAdmin.setRole(Role.ADMIN);

        project1 = new Project();
        project1.setId(222222L);
        project1.setUserId(12345L);
        project1.setName("project1");

        project2 = new Project();
        project2.setId(223323L);
        project2.setUserId(12345L);
        project2.setName("project2");

        actual.add(project1);
        actual.add(project2);

        projectRepository.add(12345L, project1);
        projectRepository.add(12345L, project2);
    }

    @After
    public void clearProjects() throws Exception {
        projectRepository.clear();
    }

    @Test
    public void testMerge() {
        @NotNull final Project project3 = new Project();
        project3.setUserId(12345L);
        project3.setName("project3");
        projectRepository.merge(project3);
        actual.add(project3);
        Assert.assertEquals(actual, projectRepository.getList());
        projectRepository.merge(project1);
        projectRepository.merge(project2);
        Assert.assertEquals(actual, projectRepository.findAll(12345L));
    }

    @Test
    public void testMerge1() {
        @NotNull final Project project4 = new Project();
        project4.setUserId(12345L);
        project4.setName("project4");
        @NotNull final Project project5 = new Project();
        project5.setUserId(12345L);
        project5.setName("project5");
        @NotNull final List<Project> expected = new ArrayList<>();
        expected.add(project4);
        expected.add(project5);
        actual.add(project4);
        actual.add(project5);
        projectRepository.merge(expected);
        Assert.assertEquals(actual, projectRepository.findAll(12345L));
        Assert.assertEquals(0, projectRepository.findAll(58621L).size());
    }

    @Test
    public void testMerge2() {
        @NotNull final Project project6 = new Project();
        project6.setUserId(58621L);
        project6.setName("project6");
        projectRepository.merge(project6);
        Assert.assertEquals(1, projectRepository.findAll(58621L).size());
    }

    @Test
    public void testClear() {
        projectRepository.clear(12345L);
        Assert.assertFalse(projectRepository.getList().size() > 0);
    }

    @Test
    public void testAdd() {
        @NotNull final Project project3 = new Project();
        project3.setUserId(12345L);
        project3.setName("project3");
        Assert.assertEquals(2, projectRepository.getList().size());
        projectRepository.add(12345L, project3);
        Assert.assertEquals(3, projectRepository.getList().size());
        Assert.assertEquals(3, projectRepository.findAll(12345L).size());
        Assert.assertEquals(0, projectRepository.findAll(58621L).size());
        actual.add(project3);
        Assert.assertEquals(actual, projectRepository.findAll(12345L));
    }

    @Test
    public void testRemove() {
        Assert.assertEquals(2, projectRepository.getList().size());
        @NotNull List<Project> expected = new ArrayList<>();
        expected.add(project2);
        projectRepository.remove(12345L, project1);
        Assert.assertEquals(1, projectRepository.getList().size());
        Assert.assertEquals(expected, projectRepository.findAll(12345L));
        projectRepository.remove(58621L, project2);
        Assert.assertEquals(expected, projectRepository.findAll(12345L));
        Assert.assertEquals(1, projectRepository.findAll(12345L).size());
        Assert.assertEquals(expected, projectRepository.getList());
    }

    @Test
    public void testFindAll() {
        @NotNull List<Project> expected = projectRepository.findAll(12345L);
        Assert.assertEquals(expected, actual);
        expected = projectRepository.findAll(58621L);
        Assert.assertEquals(0, expected.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final Long taskId = project1.getId();
        Assert.assertEquals(project1, projectRepository.findOneById(12345L, taskId));
        Assert.assertNull(projectRepository.findOneById(58621L, taskId));
        Assert.assertNull(projectRepository.findOneById(12345L, 123L));
    }

    @Test
    public void testFindOneByIndex() {
        Assert.assertEquals(project1, projectRepository.findOneByIndex(12345L, 0));
        Assert.assertEquals(project2, projectRepository.findOneByIndex(12345L, 1));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexException() {
        Assert.assertNull(projectRepository.findOneByIndex(58621L, 0));
        Assert.assertNull(projectRepository.findOneByIndex(58621L, 1));
        Assert.assertNull(projectRepository.findOneByIndex(12345L, 2));
    }

    @Test
    public void testFindOneByName() {
        Assert.assertEquals(project1, projectRepository.findOneByName(12345L, "project1"));
        Assert.assertNotEquals(project2, projectRepository.findOneByName(12345L, "project1"));
        Assert.assertNotEquals(project1, projectRepository.findOneByName(58621L, "project1"));
        Assert.assertNull(projectRepository.findOneByName(12345L, "project3"));
        Assert.assertNull(projectRepository.findOneByName(58621L, "project3"));
    }

    @Test
    public void testRemoveOneById() {
        @NotNull List<Project> expected = new ArrayList<>();
        expected.add(project2);
        @NotNull final Long taskId = project1.getId();
        projectRepository.removeOneById(12345L, taskId);
        Assert.assertNull(projectRepository.findOneById(12345L,taskId));
        Assert.assertEquals(expected, projectRepository.findAll(12345L));
        Assert.assertEquals(expected, projectRepository.getList());
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testRemoveOneByIndex() {
        @NotNull List<Project> expected = new ArrayList<>();
        expected.add(project2);
        projectRepository.removeOneByIndex(12345L, 0);
        Assert.assertEquals(project2, projectRepository.findOneByIndex(12345L,0));
        Assert.assertNull(projectRepository.findOneByIndex(12345L,1));
        Assert.assertEquals(expected, projectRepository.findAll(12345L));
        Assert.assertEquals(expected, projectRepository.getList());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull List<Project> expected = new ArrayList<>();
        expected.add(project2);
        projectRepository.removeOneByName(12345L, "project1");
        Assert.assertNull(projectRepository.findOneByName(12345L, "project1"));
        Assert.assertEquals(expected, projectRepository.findAll(12345L));
        Assert.assertEquals(expected, projectRepository.getList());
    }

}
