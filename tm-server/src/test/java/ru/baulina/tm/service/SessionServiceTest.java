package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.api.repository.ISessionRepository;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.EmptyLoginException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.util.HashUtil;
import ru.baulina.tm.util.SignatureUtil;

import java.util.Objects;

import static org.mockito.Mockito.*;

@Category({UnitTest.class, UnitServiceTest.class})
public class SessionServiceTest {

    @NotNull private static User userTest;
    @NotNull private static Session session;

    @Rule
    @NotNull
    public final TestName testName = new TestName();

    ISessionRepository mSessionRepository = Mockito.mock(ISessionRepository.class);
    IServiceLocator mServiceLocator = Mockito.mock(IServiceLocator.class);
    IUserService mUserService = Mockito.mock(IUserService.class);
    IPropertyService mPropertyService = Mockito.mock(IPropertyService.class);

    @InjectMocks
    private final ISessionService sessionService = new SessionService(mSessionRepository, mServiceLocator);

    @BeforeClass
    public static void setUsers() throws Exception {
        userTest = new User();
        userTest.setId(12345L);
        userTest.setLogin("test");
        userTest.setRole(Role.USER);
        userTest.setPasswordHash(Objects.requireNonNull(HashUtil.salt("test")));

        session = new Session();
        session.setUserId(12345L);
        session.setTimestamp(System.currentTimeMillis());
        session.setSignature(SignatureUtil.sign(session, "xyz", 120));
    }

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @Test (expected = AccessDeniedException.class)
    public void testOpenUserNull() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("test")).thenReturn(null);
        Assert.assertNotNull(sessionService.open("test", "test"));
        verify(mServiceLocator, times(2)).getUserService();
        verify(mUserService, times(2)).findByLogin("test");
    }

    @Test
    public void testOpen() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("test")).thenReturn(userTest);
        when(mServiceLocator.getPropertyService()).thenReturn(mPropertyService);
        doNothing().when(mSessionRepository).merge(isA(Session.class));
        ArgumentCaptor<Session> sessionCaptor = ArgumentCaptor.forClass(Session.class);
        Assert.assertNotNull(sessionService.open("test", "test"));
        verify(mServiceLocator, times(2)).getUserService();
        verify(mUserService, times(2)).findByLogin("test");
        verify(mServiceLocator).getPropertyService();
        verify(mSessionRepository).merge(sessionCaptor.capture());
    }

    @Test
    public void close() {
    }

    @Test
    public void closeAll() {
    }

    @Test
    public void testIsValid() {
    }

    @Test (expected = AccessDeniedException.class)
    public void testValidSessionNull() {
        sessionService.validate(null);
    }

    @Test (expected = AccessDeniedException.class)
    public void testValidRoleNull() {
        sessionService.validate(session, null);
    }

    @Test
    public void testValidate() {
        when(mServiceLocator.getPropertyService()).thenReturn(mPropertyService);
        when(mPropertyService.getSessionSalt()).thenReturn("xyz");
        when(mPropertyService.getSessionCycle()).thenReturn(120);
        when(mSessionRepository.contains(session.getId())).thenReturn(true);
        sessionService.validate(session);
        verify(mServiceLocator).getPropertyService();
        verify(mPropertyService).getSessionSalt();
        verify(mPropertyService).getSessionCycle();
        verify(mSessionRepository).contains(session.getId());
    }

    @Test
    public void testValidateRole() {
        when(mServiceLocator.getPropertyService()).thenReturn(mPropertyService);
        when(mPropertyService.getSessionSalt()).thenReturn("xyz");
        when(mPropertyService.getSessionCycle()).thenReturn(120);
        when(mSessionRepository.contains(session.getId())).thenReturn(true);
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findById(12345L)).thenReturn(userTest);
        sessionService.validate(session, Role.USER);
        verify(mServiceLocator).getPropertyService();
        verify(mPropertyService).getSessionSalt();
        verify(mPropertyService).getSessionCycle();
        verify(mSessionRepository).contains(session.getId());
        verify(mServiceLocator).getUserService();
        verify(mUserService).findById(12345L);
    }

    @Test (expected = AccessDeniedException.class)
    public void testValidateRoleException() {
        when(mServiceLocator.getPropertyService()).thenReturn(mPropertyService);
        when(mPropertyService.getSessionSalt()).thenReturn("xyz");
        when(mPropertyService.getSessionCycle()).thenReturn(120);
        when(mSessionRepository.contains(session.getId())).thenReturn(true);
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findById(12345L)).thenReturn(userTest);
        sessionService.validate(session, Role.ADMIN);
    }

    @Test
    public void testGetUser() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findById(12345L)).thenReturn(userTest);
        when(mServiceLocator.getPropertyService()).thenReturn(mPropertyService);
        when(mPropertyService.getSessionSalt()).thenReturn("xyz");
        when(mPropertyService.getSessionCycle()).thenReturn(120);
        when(mSessionRepository.contains(session.getId())).thenReturn(true);
        Assert.assertEquals(userTest, sessionService.getUser(session));
        verify(mServiceLocator).getUserService();
        verify(mUserService).findById(12345L);
        verify(mServiceLocator).getPropertyService();
        verify(mPropertyService).getSessionSalt();
        verify(mPropertyService).getSessionCycle();
        verify(mSessionRepository).contains(session.getId());
    }

    //@Test
    //public void getUserId() {
    //}

    //@Test
    //public void getListSession() {
    //}

    //@Test
    //public void testSign() {
    //    when(mServiceLocator.getUserService()).thenReturn(IPropertyService);
    //}

    @Test (expected = EmptyLoginException.class)
    public void testCheckDataAccessNullLogin() {
        sessionService.checkDataAccess(null, "test");
        sessionService.checkDataAccess("", "test");
    }

    @Test (expected = EmptyPasswordException.class)
    public void testCheckDataAccessNullPasword() {
        sessionService.checkDataAccess("test",null);
        sessionService.checkDataAccess("test","");
    }

    @Test
    public void testCheckDataAccess() {
        userTest.setLocked(false);
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("test")).thenReturn(userTest);
        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        verify(mServiceLocator).getUserService();
        verify(mUserService).findByLogin("test");
    }

    @Test (expected = AccessDeniedException.class)
    public void testCheckDataAccessUserNull() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("xy")).thenReturn(null);
        sessionService.checkDataAccess("xy", "xy");
        verify(mServiceLocator).getUserService();
        verify(mUserService).findByLogin("test");
    }

    @Test (expected = AccessDeniedException.class)
    public void testCheckDataAccessUserLoked() {
        userTest.setLocked(true);
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("test")).thenReturn(userTest);
        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        verify(mServiceLocator).getUserService();
        verify(mUserService).findByLogin("test");
    }

    @Test (expected = EmptyLoginException.class)
    public void testSignOutWithNullLoginByLogin() {
        sessionService.signOutByLogin(null);
    }

    @Test
    public void testSignOutByLogin() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("test")).thenReturn(userTest);
        doNothing().when(mSessionRepository).removeByUserId(12345L);
        sessionService.signOutByLogin("test");
        verify(mServiceLocator).getUserService();
        verify(mUserService).findByLogin("test");
        verify(mSessionRepository).removeByUserId(12345L);
    }

    @Test (expected = AccessDeniedException.class)
    public void testSignOutByLoginUserNull() {
        when(mServiceLocator.getUserService()).thenReturn(mUserService);
        when(mUserService.findByLogin("xy")).thenReturn(null);
        doNothing().when(mSessionRepository).removeByUserId(145L);
        sessionService.signOutByLogin("xy");
        verify(mServiceLocator).getUserService();
        verify(mUserService).findByLogin("xy");
        verify(mSessionRepository, never()).removeByUserId(12345L);
    }

    @Test (expected = AccessDeniedException.class)
    public void testSignOutWithNullUserIdByUserId() {
        sessionService.SignOutByUserId(null);
    }

    @Test
    public void testSignOutByUserId() {
        doNothing().when(mSessionRepository).removeByUserId(12345L);
        sessionService.SignOutByUserId(12345L);
        verify(mSessionRepository).removeByUserId(12345L);
    }

}