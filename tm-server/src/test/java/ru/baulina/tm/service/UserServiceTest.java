package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.util.HashUtil;

import static org.mockito.Mockito.*;

@Category({UnitTest.class, UnitServiceTest.class})
public class UserServiceTest {

    private static User user1;

    @Rule
    @NotNull
    public final TestName testName = new TestName();

    IUserRepository mUserRepository = Mockito.mock(IUserRepository.class);

    @InjectMocks
    private final IUserService userService = new UserService(mUserRepository);

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @BeforeClass
    public static void setUsers() throws Exception {
        user1 = new User();
        user1.setId(12345L);
        user1.setLogin("test");
        user1.setPasswordHash("14jhd54");
    }

    @Test
    public void testFindById() {
        when(mUserRepository.findById(12345L)).thenReturn(user1);
        Assert.assertEquals(user1, userService.findById(12345L));
        verify(mUserRepository).findById(12345L);
    }

    @Test (expected = EmptyIdException.class)
    public void testFindByNullId() {
        userService.findById(null);
    }

    @Test
    public void testFindByLogin() {
        when(mUserRepository.findByLogin("test")).thenReturn(user1);
        Assert.assertEquals(user1, userService.findByLogin("test"));
        verify(mUserRepository).findByLogin("test");
    }

    @Test (expected = EmptyLoginException.class)
    public void testFindByNullLogin() {
        userService.findByLogin(null);
    }

    @Test
    public void testRemoveById() {
        when(mUserRepository.removeById(12345L)).thenReturn(user1);
        Assert.assertEquals(user1, userService.removeById(12345L));
        verify(mUserRepository).removeById(12345L);
    }

    @Test (expected = EmptyIdException.class)
    public void testRemoveByNullId() {
        userService.removeById(null);
    }

    @Test
    public void testRemoveByLogin() {
        userService.removeByLogin("test");
        verify(mUserRepository).removeByLogin("test");
    }

    @Test (expected = EmptyLoginException.class)
    public void testRemoveByNullLogin() {
        userService.removeByLogin(null);
    }

    @Test
    public void testRemoveUser() {
        when(mUserRepository.removeUser(user1)).thenReturn(user1);
        Assert.assertEquals(user1, userService.removeUser(user1));
        verify(mUserRepository).removeUser(user1);
    }

    @Test
    public void testRemoveNullUser() {
        Assert.assertNull(userService.removeUser(null));
    }

    @Test (expected = EmptyLoginException.class)
    public void testCreateWithNullLogin() {
        userService.create(null, "test");
    }

    @Test (expected = EmptyLoginException.class)
    public void testCreateWithEmptyLogin() {
        userService.create("", "test");
    }

    @Test (expected = EmptyPasswordException.class)
    public void testCreateWithNullPassword() {
        userService.create("test", null);
    }

    @Test (expected = EmptyPasswordException.class)
    public void testCreateWithEmptyPassword() {
        userService.create("test", "");
    }

    @Test (expected = EmptyEmailException.class)
    public void testCreateWithEmptyEmail() {
        userService.create("test", "test", "");
    }

    @Test
    public void testCreate() {
        when(mUserRepository.add(isA(User.class))).thenReturn(user1);
        Assert.assertNotNull(userService.create("test", "test" ));
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(mUserRepository).add(userCaptor.capture());
        User savedUser = userCaptor.getValue();
        Assert.assertEquals(Role.USER, savedUser.getRole());
        Assert.assertEquals("test", savedUser.getLogin());
    }

    @Test
    public void testCreateWithEmail() {
        when(mUserRepository.add(isA(User.class))).thenReturn(user1);
        Assert.assertNotNull(userService.create("test", "test", "test@test.ru"));
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(mUserRepository).add(userCaptor.capture());
        User savedUser = userCaptor.getValue();
        Assert.assertEquals(Role.USER, savedUser.getRole());
        Assert.assertEquals("test", savedUser.getLogin());
        Assert.assertEquals("test@test.ru", savedUser.getEmail());
    }

    @Test
    public void testCreateWithRole() {
        @NotNull User user = new User();
        user.setRole(Role.ADMIN);
        user.setLogin("admin");
        user.setPasswordHash("kjh54yu");
        user.setEmail("admin@admin.ru");

        when(mUserRepository.add(isA(User.class))).thenReturn(user);
        Assert.assertNotNull(userService.create("admin", "admin", Role.ADMIN));
        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(mUserRepository).add(userCaptor.capture());
        User savedUser = userCaptor.getValue();
        Assert.assertEquals(Role.ADMIN, savedUser.getRole());
        Assert.assertEquals("admin", savedUser.getLogin());
    }

    @Test (expected = EmptyLoginException.class)
    public void testLockUserWithNullLogin() {
        userService.lockUserLogin(null);
    }

    @Test (expected = EmptyLoginException.class)
    public void testLockUserWithEmptyLogin() {
        userService.lockUserLogin("");
    }

    @Test
    public void testLockUserLogin() {
        when(mUserRepository.findByLogin("test")).thenReturn(user1);
        doNothing().when(mUserRepository).merge(user1);
        userService.lockUserLogin("test");
        verify(mUserRepository).merge(user1);
        verify(mUserRepository).findByLogin("test");
        Assert.assertTrue(user1.getLocked());
    }

    @Test (expected = EmptyLoginException.class)
    public void testUnlockUserWithNullLogin() {
        userService.unlockUserLogin(null);
    }

    @Test (expected = EmptyLoginException.class)
    public void testUnlockUserWithEmptyLogin() {
        userService.unlockUserLogin("");
    }

    @Test
    public void testUnlockUserLogin() {
        when(mUserRepository.findByLogin("test")).thenReturn(user1);
        doNothing().when(mUserRepository).merge(user1);
        userService.unlockUserLogin("test");
        verify(mUserRepository).merge(user1);
        verify(mUserRepository).findByLogin("test");
        Assert.assertFalse(user1.getLocked());
    }

    @Test (expected = AccessDeniedException.class)
    public void testChangePasswordException() {
        userService.findById(123L);
        userService.changePassword(null, "newTest", 12345L);
        userService.changePassword("oldTest", "newTest", 12345L);

    }

    @Test (expected = AccessDeniedException.class)
    public void testChangePassword() {
        doNothing().when(mUserRepository).merge(user1);
        when(mUserRepository.findById(12345L)).thenReturn(user1);
        userService.changePassword("test", "newTest", 12345L);
        verify(mUserRepository).merge(user1);
        verify(mUserRepository).findById(12345L);
        @NotNull final String hashNew = HashUtil.salt("newTest");
        Assert.assertEquals(hashNew, user1.getPasswordHash());
    }

    @Test (expected = EmptyUserIdException.class)
    public void testChangeWithNullUserId() {
        userService.changeUser("smirnov@s.ru", "Ivan", "Smirnov", null);
    }

    @Test
    public void testChangeUser() {
        doNothing().when(mUserRepository).merge(user1);
        when(mUserRepository.findById(12345L)).thenReturn(user1);
        userService.changeUser("smirnov@s.ru", "Ivan", "Smirnov", 12345L);
        verify(mUserRepository).merge(user1);
        verify(mUserRepository).findById(12345L);
        Assert.assertEquals("smirnov@s.ru", user1.getEmail());
        Assert.assertEquals("Ivan", user1.getFirstName());
        Assert.assertEquals("Smirnov", user1.getLastName());
    }

}