package ru.baulina.tm.service;

import org.junit.*;
import org.junit.rules.TestName;
import org.mockito.Mockito;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.IUserService;

import java.util.Properties;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PropertyServiceTest {

    @Rule
    public final TestName testName = new TestName();

    Properties mProperties = Mockito.mock(Properties.class);

    private final IPropertyService propertyService = new PropertyService();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void init() {

    }

    @Test
    public void getServiceHost() {
        when(mProperties.getProperty("host")).thenReturn("localhost");
        //final String envHost = System.getProperty("host");
        final String envHost = null;
        Assert.assertEquals(envHost, propertyService.getServiceHost());
        //propertyService.getServiceHost();
        //verify(mProperties).getProperty("host");
    }

    @Test
    public void getServicePort() {
    }

    @Test
    public void getSessionSalt() {
    }

    @Test
    public void getSessionCycle() {
    }

}