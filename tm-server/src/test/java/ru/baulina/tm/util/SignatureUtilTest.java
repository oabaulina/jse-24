package ru.baulina.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.UnitUtilTest;
import ru.baulina.tm.entity.Session;

@Category({UnitTest.class, UnitUtilTest.class})
public class SignatureUtilTest {

    @Rule
    public final TestName testName = new TestName();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @Test
    public void testSign() {
        @NotNull Session session = new Session();
        session.setId(123L);
        session.setUserId(14253L);
        session.setTimestamp(1611776941396L);
        @Nullable String salt = "xyz";
        @Nullable Integer iteration = 120;
        Assert.assertNotNull(SignatureUtil.sign(session, salt, iteration));
        Assert.assertEquals(
                "7cfc67094d359f35fca27899010b40c0",
                SignatureUtil.sign(session, salt, iteration)
        );
    }

    @Test
    public void testSignNulParameter() {
        @Nullable String salt = "xyz";
        @Nullable Integer iteration = 120;
        Assert.assertNull(SignatureUtil.sign(null, salt, iteration));
     }

}