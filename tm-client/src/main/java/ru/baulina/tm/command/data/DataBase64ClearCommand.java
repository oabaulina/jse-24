package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete base64 data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 DELETE]");
        @Nullable final Session session = getSession();
        endpointLocator.getAdminDampEndpoint().dataBase64Clear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
