package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID");
        @Nullable final Long id = TerminalUtil.nexLong();
        @Nullable final Session session = getSession();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().removeOneTaskById(session, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}

