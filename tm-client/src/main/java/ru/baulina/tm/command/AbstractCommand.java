package ru.baulina.tm.command;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.endpoint.IEndpointLocator;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.user.AccessDeniedException;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected IEndpointLocator endpointLocator;

    protected IServiceLocator serviceLocator;

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    public Session getSession() {
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

    @Override
    public String toString() {
        @NotNull final StringBuilder result = new StringBuilder();
        @Nullable final String nameCommand = name();
        @Nullable final String argumentCommand = arg();
        @Nullable final String descriptionCommand = description();

        if (nameCommand != null && !nameCommand.isEmpty()) result.append(nameCommand);
        if (argumentCommand != null && !argumentCommand.isEmpty()) result.append(", ").append(argumentCommand);
        if (descriptionCommand != null && !descriptionCommand.isEmpty()) result.append(": ").append(descriptionCommand);

        return result.toString();
    }

}
