package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.Session;

public final class DataJsonLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load json from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        @Nullable final Session session = getSession();
        endpointLocator.getAdminDampEndpoint().dataJasonLoad(session);
        System.out.println("[LOGOUT CURRENT USER]");
        endpointLocator.getSessionEndpoint().closeSession(session);
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        bootstrap.setSession(null);
        System.out.println("[OK]");
        System.out.println();
    }

}
