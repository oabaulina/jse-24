package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.util.TerminalUtil;

public class SessionCloseCommand extends AbstractUserCommand{

    @NotNull
    @Override
    public String name() {
        return "close-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Close session.";
    }

    @Override
    public void execute() {
        System.out.println("[CLOSE_SESSION]");
        @Nullable final Session session = getSession();
        endpointLocator.getSessionEndpoint().closeSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
