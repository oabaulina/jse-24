package ru.baulina.tm.exception.system;

public class EmptyArgumentException extends RuntimeException {

    public EmptyArgumentException() {
        super("Error! Argument is empty...");
    }

}
