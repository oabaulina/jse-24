package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;

import java.util.List;

@Category(IntegrationTest.class)
public class AdminUserEndpointIT {

    @NotNull static User userTest;
    @NotNull private static Session sessionTest;
    @NotNull private static Session sessionAdmin;

    @Rule
    public final TestName testName = new TestName();

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private static final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
    @NotNull
    private static final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
    @NotNull
    private static final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private static final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @Before
    public void setUser() throws Exception {
        System.out.println(testName.getMethodName());
        userTest = userEndpoint.createUserWithRole(
                sessionAdmin, "testUser", "testPassword", Role.USER
        );
    }

    @After
    public void clearUser() throws Exception {
        adminUserEndpoint.removeUserByLogin(sessionAdmin,"testUser");
    }

    @BeforeClass
    public static void setUsers() throws Exception {
        sessionTest = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @Test
    public void testLockUserLogin() {
        adminUserEndpoint.lockUserLogin(sessionAdmin, "testUser");
        @NotNull User userTest = adminUserEndpoint.findUserByLogin(sessionAdmin, "testUser");
        Assert.assertTrue(userTest.locked);
    }

    @Test (expected = Exception.class)
    public void testNegativeLockUserLogin() {
        adminUserEndpoint.lockUserLogin(sessionTest, userTest.getLogin());
    }

    @Test
    public void testFindUserById() {
        @NotNull final User findUserTest = adminUserEndpoint.findUserById(sessionAdmin, userTest.getId());
        Assert.assertEquals(userTest.getId(), findUserTest.getId());
    }

    @Test (expected = Exception.class)
    public void testNegativeFindUserById() {
        adminUserEndpoint.findUserById(sessionTest, userTest.getId());
    }

    @Test
    public void testGetUserList() {
        List<User> usersExpectedList = adminUserEndpoint.getUserList(sessionAdmin);
        Assert.assertEquals(3, usersExpectedList.size());
    }

    @Test (expected = Exception.class)
    public void testNegativeGetUserList() {
        adminUserEndpoint.getUserList(sessionTest);
    }

    @Test
    public void testRemoveUserByLogin() {
        Assert.assertEquals(3, adminUserEndpoint.getUserList(sessionAdmin).size());
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(sessionAdmin,"testUser"));
        adminUserEndpoint.removeUserByLogin(sessionAdmin, "testUser");
        Assert.assertEquals(2, adminUserEndpoint.getUserList(sessionAdmin).size());
        Assert.assertNull(adminUserEndpoint.findUserByLogin(sessionAdmin,"testUser"));
    }

    @Test (expected = Exception.class)
    public void testNegativeRemoveUserByLogin() {
        adminUserEndpoint.removeUserByLogin(sessionTest, userTest.getLogin());
    }

    @Test
    public void testRemoveUserById() {
        Assert.assertEquals(3, adminUserEndpoint.getUserList(sessionAdmin).size());
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(sessionAdmin,"testUser"));
        adminUserEndpoint.removeUserById(sessionAdmin, userTest.getId());
        Assert.assertEquals(2, adminUserEndpoint.getUserList(sessionAdmin).size());
        Assert.assertNull(adminUserEndpoint.findUserByLogin(sessionAdmin,"testUser"));
    }

    @Test (expected = Exception.class)
    public void testNegativeRemoveUserById() {
        adminUserEndpoint.removeUserById(sessionTest, userTest.getId());
    }

    @Test
    public void testUnlockUserLogin() {
        adminUserEndpoint.unlockUserLogin(sessionAdmin, "testUser");
        @NotNull User userTest = adminUserEndpoint.findUserByLogin(sessionAdmin, "testUser");
        Assert.assertFalse(userTest.locked);
    }

    @Test (expected = Exception.class)
    public void testNegativeUnlockUserLogin() {
        adminUserEndpoint.unlockUserLogin(sessionTest, userTest.getLogin());
    }

    @Test
    public void testFindUserByLogin() {
        @NotNull final User findUserTest = adminUserEndpoint.findUserByLogin(sessionAdmin, userTest.getLogin());
        Assert.assertEquals(userTest.getId(), findUserTest.getId());
    }

    @Test (expected = Exception.class)
    public void testNegativeFindUserByLogin() {
        adminUserEndpoint.findUserByLogin(sessionTest, userTest.getLogin());
    }

}