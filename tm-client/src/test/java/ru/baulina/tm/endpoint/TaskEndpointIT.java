package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationTest.class)
public class TaskEndpointIT {

    @NotNull static private Session sessionTest;

    @Rule
    public final TestName testName = new TestName();

    @NotNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private static final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull
    private static final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @After
    public void clearTask() throws Exception {
        taskEndpoint.clearTasks(sessionTest);
    }

    @BeforeClass
    public static void setSessions() throws Exception {
        sessionTest = sessionEndpoint.openSession("test", "test");
    }

    @AfterClass
    public static void closeSessions() throws Exception {
        sessionEndpoint.closeSession(sessionTest);
    }

    @Test
    public void testCreateTaskWithDescription() {
        @Nullable Task task = taskEndpoint.createTaskWithDescription(
                sessionTest, "task", "description"
        );
        Assert.assertNotNull(task);
        Assert.assertEquals("task", task.getName());
        Assert.assertEquals("description", task.getDescription());
   }

    @Test
    public void testAddTask() {
        @NotNull Task task = new Task();
        task.setName("task");
        task.setDescription("description");
        @NotNull Task taskCreate = taskEndpoint.addTask(sessionTest, task);
        Assert.assertEquals(task.getName(), taskCreate.getName());
    }

    @Test
    public void testFindAllTasks() {
        @NotNull Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @NotNull Task task2 = taskEndpoint.createTask(sessionTest, "task2");
        @NotNull final List<Task> listTaskExpected = taskEndpoint.findAllTasks(sessionTest);
        Assert.assertEquals(2, listTaskExpected.size());
        Assert.assertEquals(task1.getId(), listTaskExpected.get(0).getId());
        Assert.assertEquals(task2.getId(), listTaskExpected.get(1).getId());
     }

    @Test
    public void testFindOneTaskByIndex() {
        @NotNull Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @NotNull Task task2 = taskEndpoint.createTask(sessionTest, "task2");
        Assert.assertEquals(task1.getId(), taskEndpoint.findOneTaskByIndex(sessionTest, 0).getId());
        Assert.assertEquals(task2.getId(), taskEndpoint.findOneTaskByIndex(sessionTest, 1).getId());
    }

    @Test
    public void testLoadTasks() {
    }

    @Test
    public void testClearTasks() {
        taskEndpoint.createTask(sessionTest, "task1");
        taskEndpoint.createTask(sessionTest, "task2");
        Assert.assertEquals(2, taskEndpoint.findAllTasks(sessionTest).size());
        taskEndpoint.clearTasks(sessionTest);
        Assert.assertEquals(0, taskEndpoint.findAllTasks(sessionTest).size());
    }

    @Test
    public void testFindOneTaskById() {
        @NotNull Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @NotNull final Long id1 = task1.getId();
        @NotNull Task task3 = taskEndpoint.findOneTaskById(sessionTest, id1);
        @NotNull final Long id3 = task3.getId();
        Assert.assertEquals(id1, id3);
   }

    @Test
    public void testRemoveOneTaskById() {
        taskEndpoint.createTask(sessionTest, "task1");
        taskEndpoint.createTask(sessionTest, "task2");
        @Nullable Task task1 = taskEndpoint.findOneTaskByName(sessionTest, "task1");
        @Nullable Task task2 = taskEndpoint.findOneTaskByName(sessionTest, "task2");
        Assert.assertNotNull(task1);
        Assert.assertNotNull(task2);
        @NotNull final Long id1 = task1.getId();
        Assert.assertNotNull(taskEndpoint.findOneTaskById(sessionTest, id1));
        taskEndpoint.removeOneTaskById(sessionTest, task1.getId());
        Assert.assertNull(taskEndpoint.findOneTaskById(sessionTest, id1));
        @NotNull final Long id2 = task2.getId();
        Assert.assertNotNull(taskEndpoint.findOneTaskById(sessionTest, id2));
        taskEndpoint.removeOneTaskById(sessionTest, task2.getId());
        Assert.assertNull(taskEndpoint.findOneTaskById(sessionTest, id2));
    }

    @Test
    public void testUpdateTaskById() {
        taskEndpoint.createTask(sessionTest, "task");
        @NotNull Task task = taskEndpoint.findOneTaskByName(sessionTest, "task");
        @NotNull final Long id = task.getId();
        @NotNull Task taskUpdate = taskEndpoint.updateTaskById(
                sessionTest, id, "taskNew", "descriptionNew"
        );
        Assert.assertEquals("taskNew", taskUpdate.getName());
        Assert.assertEquals("descriptionNew", taskUpdate.getDescription());
    }

    @Test
    public void testUpdateTaskByIndex() {
        taskEndpoint.createTask(sessionTest, "task");
        @NotNull Task task = taskEndpoint.updateTaskByIndex(
                sessionTest, 0, "taskNew", "descriptionNew"
        );
        Assert.assertEquals("taskNew", task.getName());
        Assert.assertEquals("descriptionNew", task.getDescription());
    }

    @Test
    public void testRemoveTask() {
        @Nullable Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @Nullable Task task2 = taskEndpoint.createTask(sessionTest, "task2");
        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
        taskEndpoint.removeTask(sessionTest, task1);
        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
        taskEndpoint.removeTask(sessionTest, task2);
        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
    }

    @Test
    public void testRemoveOneTaskByIndex() {
        taskEndpoint.createTask(sessionTest, "task1");
        taskEndpoint.createTask(sessionTest, "task2");
        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
        taskEndpoint.removeOneTaskByIndex(sessionTest, 1);
        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
        taskEndpoint.removeOneTaskByIndex(sessionTest, 0);
        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
    }

    @Test
    public void testFindOneTaskByName() {
        @NotNull Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @NotNull Task task2 = taskEndpoint.createTask(sessionTest, "task2");
        @NotNull String name1 = taskEndpoint.findOneTaskByName(sessionTest, "task1").getName();
        @NotNull String name2 = taskEndpoint.findOneTaskByName(sessionTest, "task2").getName();
        Assert.assertEquals(task1.getName(), name1);
        Assert.assertEquals(task2.getName(), name2);
    }

    @Test
    public void testCreateTask() {
        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
        taskEndpoint.createTask(sessionTest, "taskNew");
        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
    }

    @Test
    public void testRemoveOneTaskByName() {
        taskEndpoint.createTask(sessionTest, "task1");
        taskEndpoint.createTask(sessionTest, "task2");
        Assert.assertEquals(2, taskEndpoint.getTaskList().size());
        Assert.assertNotNull(taskEndpoint.findOneTaskByName(sessionTest,"task1"));
        taskEndpoint.removeOneTaskByName(sessionTest, "task1");
        Assert.assertNull(taskEndpoint.findOneTaskByName(sessionTest,"task1"));
        Assert.assertEquals(1, taskEndpoint.getTaskList().size());
        Assert.assertNotNull(taskEndpoint.findOneTaskByName(sessionTest,"task2"));
        taskEndpoint.removeOneTaskByName(sessionTest, "task2");
        Assert.assertEquals(0, taskEndpoint.getTaskList().size());
    }

    @Test
    public void testGetTaskList() {
        @NotNull Task task1 = taskEndpoint.createTask(sessionTest, "task1");
        @NotNull Task task2 = taskEndpoint.createTask(sessionTest, "task2");
        @Nullable final List<Long> expected = new ArrayList<>();
        expected.add(task1.getId());
        expected.add(task2.getId());
        @NotNull final List<Task> tasks = taskEndpoint.getTaskList();
        @NotNull final List<Long> actual = new ArrayList<>();
        for (@NotNull final Task task: tasks) {
            actual.add(task.getId());
        }
        Assert.assertEquals(expected, actual);
    }

}